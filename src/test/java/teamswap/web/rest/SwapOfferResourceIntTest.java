package teamswap.web.rest;

import teamswap.SwapThatApp;

import teamswap.domain.SwapOffer;
import teamswap.repository.SwapOfferRepository;
import teamswap.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static teamswap.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import teamswap.domain.enumeration.SwapStatus;
/**
 * Test class for the SwapOfferResource REST controller.
 *
 * @see SwapOfferResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SwapThatApp.class)
public class SwapOfferResourceIntTest {

    private static final SwapStatus DEFAULT_STATUS = SwapStatus.ACCEPTED;
    private static final SwapStatus UPDATED_STATUS = SwapStatus.PENDING;

    @Autowired
    private SwapOfferRepository swapOfferRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSwapOfferMockMvc;

    private SwapOffer swapOffer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SwapOfferResource swapOfferResource = new SwapOfferResource(swapOfferRepository);
        this.restSwapOfferMockMvc = MockMvcBuilders.standaloneSetup(swapOfferResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SwapOffer createEntity(EntityManager em) {
        SwapOffer swapOffer = new SwapOffer()
            .status(DEFAULT_STATUS);
        return swapOffer;
    }

    @Before
    public void initTest() {
        swapOffer = createEntity(em);
    }

    @Test
    @Transactional
    public void createSwapOffer() throws Exception {
        int databaseSizeBeforeCreate = swapOfferRepository.findAll().size();

        // Create the SwapOffer
        restSwapOfferMockMvc.perform(post("/api/swap-offers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(swapOffer)))
            .andExpect(status().isCreated());

        // Validate the SwapOffer in the database
        List<SwapOffer> swapOfferList = swapOfferRepository.findAll();
        assertThat(swapOfferList).hasSize(databaseSizeBeforeCreate + 1);
        SwapOffer testSwapOffer = swapOfferList.get(swapOfferList.size() - 1);
        assertThat(testSwapOffer.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createSwapOfferWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = swapOfferRepository.findAll().size();

        // Create the SwapOffer with an existing ID
        swapOffer.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSwapOfferMockMvc.perform(post("/api/swap-offers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(swapOffer)))
            .andExpect(status().isBadRequest());

        // Validate the SwapOffer in the database
        List<SwapOffer> swapOfferList = swapOfferRepository.findAll();
        assertThat(swapOfferList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSwapOffers() throws Exception {
        // Initialize the database
        swapOfferRepository.saveAndFlush(swapOffer);

        // Get all the swapOfferList
        restSwapOfferMockMvc.perform(get("/api/swap-offers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(swapOffer.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }
    
    @Test
    @Transactional
    public void getSwapOffer() throws Exception {
        // Initialize the database
        swapOfferRepository.saveAndFlush(swapOffer);

        // Get the swapOffer
        restSwapOfferMockMvc.perform(get("/api/swap-offers/{id}", swapOffer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(swapOffer.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSwapOffer() throws Exception {
        // Get the swapOffer
        restSwapOfferMockMvc.perform(get("/api/swap-offers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSwapOffer() throws Exception {
        // Initialize the database
        swapOfferRepository.saveAndFlush(swapOffer);

        int databaseSizeBeforeUpdate = swapOfferRepository.findAll().size();

        // Update the swapOffer
        SwapOffer updatedSwapOffer = swapOfferRepository.findById(swapOffer.getId()).get();
        // Disconnect from session so that the updates on updatedSwapOffer are not directly saved in db
        em.detach(updatedSwapOffer);
        updatedSwapOffer
            .status(UPDATED_STATUS);

        restSwapOfferMockMvc.perform(put("/api/swap-offers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSwapOffer)))
            .andExpect(status().isOk());

        // Validate the SwapOffer in the database
        List<SwapOffer> swapOfferList = swapOfferRepository.findAll();
        assertThat(swapOfferList).hasSize(databaseSizeBeforeUpdate);
        SwapOffer testSwapOffer = swapOfferList.get(swapOfferList.size() - 1);
        assertThat(testSwapOffer.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingSwapOffer() throws Exception {
        int databaseSizeBeforeUpdate = swapOfferRepository.findAll().size();

        // Create the SwapOffer

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSwapOfferMockMvc.perform(put("/api/swap-offers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(swapOffer)))
            .andExpect(status().isBadRequest());

        // Validate the SwapOffer in the database
        List<SwapOffer> swapOfferList = swapOfferRepository.findAll();
        assertThat(swapOfferList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSwapOffer() throws Exception {
        // Initialize the database
        swapOfferRepository.saveAndFlush(swapOffer);

        int databaseSizeBeforeDelete = swapOfferRepository.findAll().size();

        // Delete the swapOffer
        restSwapOfferMockMvc.perform(delete("/api/swap-offers/{id}", swapOffer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SwapOffer> swapOfferList = swapOfferRepository.findAll();
        assertThat(swapOfferList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SwapOffer.class);
        SwapOffer swapOffer1 = new SwapOffer();
        swapOffer1.setId(1L);
        SwapOffer swapOffer2 = new SwapOffer();
        swapOffer2.setId(swapOffer1.getId());
        assertThat(swapOffer1).isEqualTo(swapOffer2);
        swapOffer2.setId(2L);
        assertThat(swapOffer1).isNotEqualTo(swapOffer2);
        swapOffer1.setId(null);
        assertThat(swapOffer1).isNotEqualTo(swapOffer2);
    }
}
