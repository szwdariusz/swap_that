package teamswap.web.rest;

import teamswap.SwapThatApp;

import teamswap.domain.NotificationContent;
import teamswap.repository.NotificationContentRepository;
import teamswap.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static teamswap.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NotificationContentResource REST controller.
 *
 * @see NotificationContentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SwapThatApp.class)
public class NotificationContentResourceIntTest {

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    @Autowired
    private NotificationContentRepository notificationContentRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNotificationContentMockMvc;

    private NotificationContent notificationContent;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NotificationContentResource notificationContentResource = new NotificationContentResource(notificationContentRepository);
        this.restNotificationContentMockMvc = MockMvcBuilders.standaloneSetup(notificationContentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NotificationContent createEntity(EntityManager em) {
        NotificationContent notificationContent = new NotificationContent()
            .message(DEFAULT_MESSAGE);
        return notificationContent;
    }

    @Before
    public void initTest() {
        notificationContent = createEntity(em);
    }

    @Test
    @Transactional
    public void createNotificationContent() throws Exception {
        int databaseSizeBeforeCreate = notificationContentRepository.findAll().size();

        // Create the NotificationContent
        restNotificationContentMockMvc.perform(post("/api/notification-contents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationContent)))
            .andExpect(status().isCreated());

        // Validate the NotificationContent in the database
        List<NotificationContent> notificationContentList = notificationContentRepository.findAll();
        assertThat(notificationContentList).hasSize(databaseSizeBeforeCreate + 1);
        NotificationContent testNotificationContent = notificationContentList.get(notificationContentList.size() - 1);
        assertThat(testNotificationContent.getMessage()).isEqualTo(DEFAULT_MESSAGE);
    }

    @Test
    @Transactional
    public void createNotificationContentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = notificationContentRepository.findAll().size();

        // Create the NotificationContent with an existing ID
        notificationContent.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNotificationContentMockMvc.perform(post("/api/notification-contents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationContent)))
            .andExpect(status().isBadRequest());

        // Validate the NotificationContent in the database
        List<NotificationContent> notificationContentList = notificationContentRepository.findAll();
        assertThat(notificationContentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNotificationContents() throws Exception {
        // Initialize the database
        notificationContentRepository.saveAndFlush(notificationContent);

        // Get all the notificationContentList
        restNotificationContentMockMvc.perform(get("/api/notification-contents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notificationContent.getId().intValue())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())));
    }
    
    @Test
    @Transactional
    public void getNotificationContent() throws Exception {
        // Initialize the database
        notificationContentRepository.saveAndFlush(notificationContent);

        // Get the notificationContent
        restNotificationContentMockMvc.perform(get("/api/notification-contents/{id}", notificationContent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(notificationContent.getId().intValue()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingNotificationContent() throws Exception {
        // Get the notificationContent
        restNotificationContentMockMvc.perform(get("/api/notification-contents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNotificationContent() throws Exception {
        // Initialize the database
        notificationContentRepository.saveAndFlush(notificationContent);

        int databaseSizeBeforeUpdate = notificationContentRepository.findAll().size();

        // Update the notificationContent
        NotificationContent updatedNotificationContent = notificationContentRepository.findById(notificationContent.getId()).get();
        // Disconnect from session so that the updates on updatedNotificationContent are not directly saved in db
        em.detach(updatedNotificationContent);
        updatedNotificationContent
            .message(UPDATED_MESSAGE);

        restNotificationContentMockMvc.perform(put("/api/notification-contents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNotificationContent)))
            .andExpect(status().isOk());

        // Validate the NotificationContent in the database
        List<NotificationContent> notificationContentList = notificationContentRepository.findAll();
        assertThat(notificationContentList).hasSize(databaseSizeBeforeUpdate);
        NotificationContent testNotificationContent = notificationContentList.get(notificationContentList.size() - 1);
        assertThat(testNotificationContent.getMessage()).isEqualTo(UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingNotificationContent() throws Exception {
        int databaseSizeBeforeUpdate = notificationContentRepository.findAll().size();

        // Create the NotificationContent

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotificationContentMockMvc.perform(put("/api/notification-contents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationContent)))
            .andExpect(status().isBadRequest());

        // Validate the NotificationContent in the database
        List<NotificationContent> notificationContentList = notificationContentRepository.findAll();
        assertThat(notificationContentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNotificationContent() throws Exception {
        // Initialize the database
        notificationContentRepository.saveAndFlush(notificationContent);

        int databaseSizeBeforeDelete = notificationContentRepository.findAll().size();

        // Delete the notificationContent
        restNotificationContentMockMvc.perform(delete("/api/notification-contents/{id}", notificationContent.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<NotificationContent> notificationContentList = notificationContentRepository.findAll();
        assertThat(notificationContentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NotificationContent.class);
        NotificationContent notificationContent1 = new NotificationContent();
        notificationContent1.setId(1L);
        NotificationContent notificationContent2 = new NotificationContent();
        notificationContent2.setId(notificationContent1.getId());
        assertThat(notificationContent1).isEqualTo(notificationContent2);
        notificationContent2.setId(2L);
        assertThat(notificationContent1).isNotEqualTo(notificationContent2);
        notificationContent1.setId(null);
        assertThat(notificationContent1).isNotEqualTo(notificationContent2);
    }
}
