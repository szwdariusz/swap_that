package teamswap.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import teamswap.domain.enumeration.SwapStatus;

/**
 * A SwapOffer.
 */
@Entity
@Table(name = "swap_offer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SwapOffer implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private SwapStatus status;

    @OneToOne
    @JoinColumn(unique = true)
    private Item offererItem;

    @OneToOne
    @JoinColumn(unique = true)
    private Item offereeItem;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SwapStatus getStatus() {
        return status;
    }

    public SwapOffer status(SwapStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(SwapStatus status) {
        this.status = status;
    }

    public Item getOffererItem() {
        return offererItem;
    }

    public SwapOffer offererItem(Item item) {
        this.offererItem = item;
        return this;
    }

    public void setOffererItem(Item item) {
        this.offererItem = item;
    }

    public Item getOffereeItem() {
        return offereeItem;
    }

    public SwapOffer offereeItem(Item item) {
        this.offereeItem = item;
        return this;
    }

    public void setOffereeItem(Item item) {
        this.offereeItem = item;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SwapOffer swapOffer = (SwapOffer) o;
        if (swapOffer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), swapOffer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SwapOffer{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
