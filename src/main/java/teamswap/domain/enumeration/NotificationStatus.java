package teamswap.domain.enumeration;

/**
 * The NotificationStatus enumeration.
 */
public enum NotificationStatus {
    SENT, PENDING, FAILED
}
