package teamswap.domain.enumeration;

/**
 * The SwapStatus enumeration.
 */
public enum SwapStatus {
    ACCEPTED, PENDING, REJECTED
}
