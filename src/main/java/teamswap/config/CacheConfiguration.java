package teamswap.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(teamswap.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(teamswap.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(teamswap.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(teamswap.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(teamswap.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(teamswap.domain.Item.class.getName(), jcacheConfiguration);
            cm.createCache(teamswap.domain.Image.class.getName(), jcacheConfiguration);
            cm.createCache(teamswap.domain.Category.class.getName(), jcacheConfiguration);
            cm.createCache(teamswap.domain.SwapOffer.class.getName(), jcacheConfiguration);
            cm.createCache(teamswap.domain.Notification.class.getName(), jcacheConfiguration);
            cm.createCache(teamswap.domain.NotificationContent.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
