package teamswap.web.rest;
import teamswap.domain.SwapOffer;
import teamswap.repository.SwapOfferRepository;
import teamswap.web.rest.errors.BadRequestAlertException;
import teamswap.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SwapOffer.
 */
@RestController
@RequestMapping("/api")
public class SwapOfferResource {

    private final Logger log = LoggerFactory.getLogger(SwapOfferResource.class);

    private static final String ENTITY_NAME = "swapOffer";

    private final SwapOfferRepository swapOfferRepository;

    public SwapOfferResource(SwapOfferRepository swapOfferRepository) {
        this.swapOfferRepository = swapOfferRepository;
    }

    /**
     * POST  /swap-offers : Create a new swapOffer.
     *
     * @param swapOffer the swapOffer to create
     * @return the ResponseEntity with status 201 (Created) and with body the new swapOffer, or with status 400 (Bad Request) if the swapOffer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/swap-offers")
    public ResponseEntity<SwapOffer> createSwapOffer(@RequestBody SwapOffer swapOffer) throws URISyntaxException {
        log.debug("REST request to save SwapOffer : {}", swapOffer);
        if (swapOffer.getId() != null) {
            throw new BadRequestAlertException("A new swapOffer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SwapOffer result = swapOfferRepository.save(swapOffer);
        return ResponseEntity.created(new URI("/api/swap-offers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /swap-offers : Updates an existing swapOffer.
     *
     * @param swapOffer the swapOffer to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated swapOffer,
     * or with status 400 (Bad Request) if the swapOffer is not valid,
     * or with status 500 (Internal Server Error) if the swapOffer couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/swap-offers")
    public ResponseEntity<SwapOffer> updateSwapOffer(@RequestBody SwapOffer swapOffer) throws URISyntaxException {
        log.debug("REST request to update SwapOffer : {}", swapOffer);
        if (swapOffer.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SwapOffer result = swapOfferRepository.save(swapOffer);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, swapOffer.getId().toString()))
            .body(result);
    }

    /**
     * GET  /swap-offers : get all the swapOffers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of swapOffers in body
     */
    @GetMapping("/swap-offers")
    public List<SwapOffer> getAllSwapOffers() {
        log.debug("REST request to get all SwapOffers");
        return swapOfferRepository.findAll();
    }

    /**
     * GET  /swap-offers/:id : get the "id" swapOffer.
     *
     * @param id the id of the swapOffer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the swapOffer, or with status 404 (Not Found)
     */
    @GetMapping("/swap-offers/{id}")
    public ResponseEntity<SwapOffer> getSwapOffer(@PathVariable Long id) {
        log.debug("REST request to get SwapOffer : {}", id);
        Optional<SwapOffer> swapOffer = swapOfferRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(swapOffer);
    }

    /**
     * DELETE  /swap-offers/:id : delete the "id" swapOffer.
     *
     * @param id the id of the swapOffer to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/swap-offers/{id}")
    public ResponseEntity<Void> deleteSwapOffer(@PathVariable Long id) {
        log.debug("REST request to delete SwapOffer : {}", id);
        swapOfferRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
