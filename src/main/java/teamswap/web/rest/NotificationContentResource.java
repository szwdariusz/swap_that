package teamswap.web.rest;
import teamswap.domain.NotificationContent;
import teamswap.repository.NotificationContentRepository;
import teamswap.web.rest.errors.BadRequestAlertException;
import teamswap.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing NotificationContent.
 */
@RestController
@RequestMapping("/api")
public class NotificationContentResource {

    private final Logger log = LoggerFactory.getLogger(NotificationContentResource.class);

    private static final String ENTITY_NAME = "notificationContent";

    private final NotificationContentRepository notificationContentRepository;

    public NotificationContentResource(NotificationContentRepository notificationContentRepository) {
        this.notificationContentRepository = notificationContentRepository;
    }

    /**
     * POST  /notification-contents : Create a new notificationContent.
     *
     * @param notificationContent the notificationContent to create
     * @return the ResponseEntity with status 201 (Created) and with body the new notificationContent, or with status 400 (Bad Request) if the notificationContent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/notification-contents")
    public ResponseEntity<NotificationContent> createNotificationContent(@RequestBody NotificationContent notificationContent) throws URISyntaxException {
        log.debug("REST request to save NotificationContent : {}", notificationContent);
        if (notificationContent.getId() != null) {
            throw new BadRequestAlertException("A new notificationContent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NotificationContent result = notificationContentRepository.save(notificationContent);
        return ResponseEntity.created(new URI("/api/notification-contents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /notification-contents : Updates an existing notificationContent.
     *
     * @param notificationContent the notificationContent to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated notificationContent,
     * or with status 400 (Bad Request) if the notificationContent is not valid,
     * or with status 500 (Internal Server Error) if the notificationContent couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/notification-contents")
    public ResponseEntity<NotificationContent> updateNotificationContent(@RequestBody NotificationContent notificationContent) throws URISyntaxException {
        log.debug("REST request to update NotificationContent : {}", notificationContent);
        if (notificationContent.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NotificationContent result = notificationContentRepository.save(notificationContent);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, notificationContent.getId().toString()))
            .body(result);
    }

    /**
     * GET  /notification-contents : get all the notificationContents.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of notificationContents in body
     */
    @GetMapping("/notification-contents")
    public List<NotificationContent> getAllNotificationContents() {
        log.debug("REST request to get all NotificationContents");
        return notificationContentRepository.findAll();
    }

    /**
     * GET  /notification-contents/:id : get the "id" notificationContent.
     *
     * @param id the id of the notificationContent to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the notificationContent, or with status 404 (Not Found)
     */
    @GetMapping("/notification-contents/{id}")
    public ResponseEntity<NotificationContent> getNotificationContent(@PathVariable Long id) {
        log.debug("REST request to get NotificationContent : {}", id);
        Optional<NotificationContent> notificationContent = notificationContentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(notificationContent);
    }

    /**
     * DELETE  /notification-contents/:id : delete the "id" notificationContent.
     *
     * @param id the id of the notificationContent to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/notification-contents/{id}")
    public ResponseEntity<Void> deleteNotificationContent(@PathVariable Long id) {
        log.debug("REST request to delete NotificationContent : {}", id);
        notificationContentRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
