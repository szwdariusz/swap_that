/**
 * View Models used by Spring MVC REST controllers.
 */
package teamswap.web.rest.vm;
