package teamswap.repository;

import teamswap.domain.NotificationContent;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NotificationContent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationContentRepository extends JpaRepository<NotificationContent, Long> {

}
