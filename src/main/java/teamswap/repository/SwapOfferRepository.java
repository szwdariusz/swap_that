package teamswap.repository;

import teamswap.domain.SwapOffer;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SwapOffer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SwapOfferRepository extends JpaRepository<SwapOffer, Long> {

}
