import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './swap-offer.reducer';
import { ISwapOffer } from 'app/shared/model/swap-offer.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISwapOfferDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class SwapOfferDetail extends React.Component<ISwapOfferDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { swapOfferEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="swapThatApp.swapOffer.detail.title">SwapOffer</Translate> [<b>{swapOfferEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="status">
                <Translate contentKey="swapThatApp.swapOffer.status">Status</Translate>
              </span>
            </dt>
            <dd>{swapOfferEntity.status}</dd>
            <dt>
              <Translate contentKey="swapThatApp.swapOffer.offererItem">Offerer Item</Translate>
            </dt>
            <dd>{swapOfferEntity.offererItem ? swapOfferEntity.offererItem.id : ''}</dd>
            <dt>
              <Translate contentKey="swapThatApp.swapOffer.offereeItem">Offeree Item</Translate>
            </dt>
            <dd>{swapOfferEntity.offereeItem ? swapOfferEntity.offereeItem.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/swap-offer" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/swap-offer/${swapOfferEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ swapOffer }: IRootState) => ({
  swapOfferEntity: swapOffer.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SwapOfferDetail);
