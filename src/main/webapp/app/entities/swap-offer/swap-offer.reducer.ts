import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ISwapOffer, defaultValue } from 'app/shared/model/swap-offer.model';

export const ACTION_TYPES = {
  FETCH_SWAPOFFER_LIST: 'swapOffer/FETCH_SWAPOFFER_LIST',
  FETCH_SWAPOFFER: 'swapOffer/FETCH_SWAPOFFER',
  CREATE_SWAPOFFER: 'swapOffer/CREATE_SWAPOFFER',
  UPDATE_SWAPOFFER: 'swapOffer/UPDATE_SWAPOFFER',
  DELETE_SWAPOFFER: 'swapOffer/DELETE_SWAPOFFER',
  RESET: 'swapOffer/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISwapOffer>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type SwapOfferState = Readonly<typeof initialState>;

// Reducer

export default (state: SwapOfferState = initialState, action): SwapOfferState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SWAPOFFER_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SWAPOFFER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SWAPOFFER):
    case REQUEST(ACTION_TYPES.UPDATE_SWAPOFFER):
    case REQUEST(ACTION_TYPES.DELETE_SWAPOFFER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SWAPOFFER_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SWAPOFFER):
    case FAILURE(ACTION_TYPES.CREATE_SWAPOFFER):
    case FAILURE(ACTION_TYPES.UPDATE_SWAPOFFER):
    case FAILURE(ACTION_TYPES.DELETE_SWAPOFFER):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SWAPOFFER_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_SWAPOFFER):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SWAPOFFER):
    case SUCCESS(ACTION_TYPES.UPDATE_SWAPOFFER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SWAPOFFER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/swap-offers';

// Actions

export const getEntities: ICrudGetAllAction<ISwapOffer> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_SWAPOFFER_LIST,
  payload: axios.get<ISwapOffer>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<ISwapOffer> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SWAPOFFER,
    payload: axios.get<ISwapOffer>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ISwapOffer> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SWAPOFFER,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ISwapOffer> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SWAPOFFER,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ISwapOffer> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SWAPOFFER,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
