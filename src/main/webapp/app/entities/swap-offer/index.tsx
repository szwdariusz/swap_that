import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import SwapOffer from './swap-offer';
import SwapOfferDetail from './swap-offer-detail';
import SwapOfferUpdate from './swap-offer-update';
import SwapOfferDeleteDialog from './swap-offer-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SwapOfferUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SwapOfferUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SwapOfferDetail} />
      <ErrorBoundaryRoute path={match.url} component={SwapOffer} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={SwapOfferDeleteDialog} />
  </>
);

export default Routes;
