import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './swap-offer.reducer';
import { ISwapOffer } from 'app/shared/model/swap-offer.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISwapOfferProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class SwapOffer extends React.Component<ISwapOfferProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { swapOfferList, match } = this.props;
    return (
      <div>
        <h2 id="swap-offer-heading">
          <Translate contentKey="swapThatApp.swapOffer.home.title">Swap Offers</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="swapThatApp.swapOffer.home.createLabel">Create new Swap Offer</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="swapThatApp.swapOffer.status">Status</Translate>
                </th>
                <th>
                  <Translate contentKey="swapThatApp.swapOffer.offererItem">Offerer Item</Translate>
                </th>
                <th>
                  <Translate contentKey="swapThatApp.swapOffer.offereeItem">Offeree Item</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {swapOfferList.map((swapOffer, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${swapOffer.id}`} color="link" size="sm">
                      {swapOffer.id}
                    </Button>
                  </td>
                  <td>
                    <Translate contentKey={`swapThatApp.SwapStatus.${swapOffer.status}`} />
                  </td>
                  <td>{swapOffer.offererItem ? <Link to={`item/${swapOffer.offererItem.id}`}>{swapOffer.offererItem.id}</Link> : ''}</td>
                  <td>{swapOffer.offereeItem ? <Link to={`item/${swapOffer.offereeItem.id}`}>{swapOffer.offereeItem.id}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${swapOffer.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${swapOffer.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${swapOffer.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ swapOffer }: IRootState) => ({
  swapOfferList: swapOffer.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SwapOffer);
