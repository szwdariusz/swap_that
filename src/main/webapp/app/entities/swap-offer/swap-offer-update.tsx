import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IItem } from 'app/shared/model/item.model';
import { getEntities as getItems } from 'app/entities/item/item.reducer';
import { getEntity, updateEntity, createEntity, reset } from './swap-offer.reducer';
import { ISwapOffer } from 'app/shared/model/swap-offer.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ISwapOfferUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ISwapOfferUpdateState {
  isNew: boolean;
  offererItemId: string;
  offereeItemId: string;
}

export class SwapOfferUpdate extends React.Component<ISwapOfferUpdateProps, ISwapOfferUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      offererItemId: '0',
      offereeItemId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getItems();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { swapOfferEntity } = this.props;
      const entity = {
        ...swapOfferEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/swap-offer');
  };

  render() {
    const { swapOfferEntity, items, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="swapThatApp.swapOffer.home.createOrEditLabel">
              <Translate contentKey="swapThatApp.swapOffer.home.createOrEditLabel">Create or edit a SwapOffer</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : swapOfferEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="swap-offer-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="statusLabel">
                    <Translate contentKey="swapThatApp.swapOffer.status">Status</Translate>
                  </Label>
                  <AvInput
                    id="swap-offer-status"
                    type="select"
                    className="form-control"
                    name="status"
                    value={(!isNew && swapOfferEntity.status) || 'ACCEPTED'}
                  >
                    <option value="ACCEPTED">
                      <Translate contentKey="swapThatApp.SwapStatus.ACCEPTED" />
                    </option>
                    <option value="PENDING">
                      <Translate contentKey="swapThatApp.SwapStatus.PENDING" />
                    </option>
                    <option value="REJECTED">
                      <Translate contentKey="swapThatApp.SwapStatus.REJECTED" />
                    </option>
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="offererItem.id">
                    <Translate contentKey="swapThatApp.swapOffer.offererItem">Offerer Item</Translate>
                  </Label>
                  <AvInput id="swap-offer-offererItem" type="select" className="form-control" name="offererItem.id">
                    <option value="" key="0" />
                    {items
                      ? items.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="offereeItem.id">
                    <Translate contentKey="swapThatApp.swapOffer.offereeItem">Offeree Item</Translate>
                  </Label>
                  <AvInput id="swap-offer-offereeItem" type="select" className="form-control" name="offereeItem.id">
                    <option value="" key="0" />
                    {items
                      ? items.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/swap-offer" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  items: storeState.item.entities,
  swapOfferEntity: storeState.swapOffer.entity,
  loading: storeState.swapOffer.loading,
  updating: storeState.swapOffer.updating,
  updateSuccess: storeState.swapOffer.updateSuccess
});

const mapDispatchToProps = {
  getItems,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SwapOfferUpdate);
