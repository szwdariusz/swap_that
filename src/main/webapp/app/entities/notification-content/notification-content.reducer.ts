import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { INotificationContent, defaultValue } from 'app/shared/model/notification-content.model';

export const ACTION_TYPES = {
  FETCH_NOTIFICATIONCONTENT_LIST: 'notificationContent/FETCH_NOTIFICATIONCONTENT_LIST',
  FETCH_NOTIFICATIONCONTENT: 'notificationContent/FETCH_NOTIFICATIONCONTENT',
  CREATE_NOTIFICATIONCONTENT: 'notificationContent/CREATE_NOTIFICATIONCONTENT',
  UPDATE_NOTIFICATIONCONTENT: 'notificationContent/UPDATE_NOTIFICATIONCONTENT',
  DELETE_NOTIFICATIONCONTENT: 'notificationContent/DELETE_NOTIFICATIONCONTENT',
  RESET: 'notificationContent/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<INotificationContent>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type NotificationContentState = Readonly<typeof initialState>;

// Reducer

export default (state: NotificationContentState = initialState, action): NotificationContentState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_NOTIFICATIONCONTENT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_NOTIFICATIONCONTENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_NOTIFICATIONCONTENT):
    case REQUEST(ACTION_TYPES.UPDATE_NOTIFICATIONCONTENT):
    case REQUEST(ACTION_TYPES.DELETE_NOTIFICATIONCONTENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_NOTIFICATIONCONTENT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_NOTIFICATIONCONTENT):
    case FAILURE(ACTION_TYPES.CREATE_NOTIFICATIONCONTENT):
    case FAILURE(ACTION_TYPES.UPDATE_NOTIFICATIONCONTENT):
    case FAILURE(ACTION_TYPES.DELETE_NOTIFICATIONCONTENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_NOTIFICATIONCONTENT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_NOTIFICATIONCONTENT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_NOTIFICATIONCONTENT):
    case SUCCESS(ACTION_TYPES.UPDATE_NOTIFICATIONCONTENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_NOTIFICATIONCONTENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/notification-contents';

// Actions

export const getEntities: ICrudGetAllAction<INotificationContent> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_NOTIFICATIONCONTENT_LIST,
  payload: axios.get<INotificationContent>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<INotificationContent> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_NOTIFICATIONCONTENT,
    payload: axios.get<INotificationContent>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<INotificationContent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_NOTIFICATIONCONTENT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<INotificationContent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_NOTIFICATIONCONTENT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<INotificationContent> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NOTIFICATIONCONTENT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
