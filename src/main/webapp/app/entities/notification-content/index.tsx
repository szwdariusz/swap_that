import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import NotificationContent from './notification-content';
import NotificationContentDetail from './notification-content-detail';
import NotificationContentUpdate from './notification-content-update';
import NotificationContentDeleteDialog from './notification-content-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={NotificationContentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={NotificationContentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={NotificationContentDetail} />
      <ErrorBoundaryRoute path={match.url} component={NotificationContent} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={NotificationContentDeleteDialog} />
  </>
);

export default Routes;
