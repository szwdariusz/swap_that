import { IItem } from 'app/shared/model/item.model';

export const enum SwapStatus {
  ACCEPTED = 'ACCEPTED',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED'
}

export interface ISwapOffer {
  id?: number;
  status?: SwapStatus;
  offererItem?: IItem;
  offereeItem?: IItem;
}

export const defaultValue: Readonly<ISwapOffer> = {};
