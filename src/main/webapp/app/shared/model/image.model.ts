export interface IImage {
  id?: number;
  url?: string;
}

export const defaultValue: Readonly<IImage> = {};
