import { IUser } from 'app/shared/model/user.model';
import { INotificationContent } from 'app/shared/model/notification-content.model';

export const enum NotificationStatus {
  SENT = 'SENT',
  PENDING = 'PENDING',
  FAILED = 'FAILED'
}

export interface INotification {
  id?: number;
  status?: NotificationStatus;
  receiver?: IUser;
  content?: INotificationContent;
}

export const defaultValue: Readonly<INotification> = {};
