export interface INotificationContent {
  id?: number;
  message?: string;
}

export const defaultValue: Readonly<INotificationContent> = {};
