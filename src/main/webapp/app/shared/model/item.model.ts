import { IImage } from 'app/shared/model/image.model';
import { ICategory } from 'app/shared/model/category.model';

export interface IItem {
  id?: number;
  name?: string;
  description?: string;
  userId?: number;
  image?: IImage;
  category?: ICategory;
}

export const defaultValue: Readonly<IItem> = {};
